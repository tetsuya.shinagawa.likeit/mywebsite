<%@	page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<meta charset="UTF-8">
<title>利用履歴</title>
</head>

<body>

    <div class="container">
			<h2 class="offset-sm-4 col-sm-8">利用履歴</h2>

						<table class="table">
							<thead>
								<tr>
									<th scope="col">利用日時</th>
									<th scope="col">受け入れチーム</th>
                                    <th scope="col">利用リンク</th>
                                    <th scope="col">出欠</th>
                                    <th scope="col">備考</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td>${boughtDataDetail.formatDate}</td>
									<td>${boughtDataDetail.deliveryMethodName}</td>
									<td>${boughtDataDetail.totalPrice + boughtDataDetail.deliveryMethodPrice} 円</td>
                                    <td>出欠</td>
                                    <td>bikou</td>
								</tr>
                            </tbody>
						</table>
            </div>



        <nav class="nav offset-sm-2">
            <a href="" class="nav-link">応募プレイヤー一覧へ戻る</a>
        <!--チームにのみ表示-->
            <a href="" class="nav-link">マイページへ戻る</a>
            <!--プレイヤーにのみ表示-->
    </nav>
</body>
</html>