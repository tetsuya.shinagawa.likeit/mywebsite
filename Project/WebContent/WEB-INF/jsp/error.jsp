<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>システムエラー</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
	<div class="container">
		<div class="row">
		 <div class="offset-4">
							<h4 class="red-text">システムエラーが発生しました</h4>
							<h5 class="red-text">${errorMessage}</h5>
        </div>
        <div class="row">
            </div>
								<div class="offset-2">
									<a href="Index">TOPページへ</a>
								</div>
        </div>
        </div>

</body>
</html>