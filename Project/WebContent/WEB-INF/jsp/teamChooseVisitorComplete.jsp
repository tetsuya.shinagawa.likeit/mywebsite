<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <title>確定報告</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <head>
        <div>${UserList.name}</div>
      </head>

    <body>

        <h1 class="container row offset-md-4 col-md-8">
        受付を完了しました。
        </h1>

        <div class="container-sm row offset-md-2 col-md-4">
            <a href="">トップページへ戻る</a>
        </div>


    </body>
    </html>