<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>トップ</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <head><h2 class="offset-sm-5">トップページ</h2></head>

	<body>
        <header>
  <nav class="nav nav-pills nav-justified">
  <a class="nav-item nav-link" href="#">マイページ</a>
  <a class="nav-item nav-link" href="#">ビジターを探す</a>
  <a class="nav-item nav-link" href="#">チームページ</a>
  <a class="nav-item nav-link" href="#">募集情報を掲載する</a>
</nav>

        </header>
        <br>

		 <h3 class="offset-sm-4">ビジター情報ランダム表示</h3>

						<table class="table-dark">
                            <thead>
								<tr>
                                    <th scope="col">募集内容</th>
									<th scope="col">利用日時</th>
									<th scope="col"> チーム</th>
                                    <th scope="col">場所</th>
									<th scope="col">募集人数</th>
                                    <th scope="col">詳細</th>
								</tr>

                             </thead>

							<tbody>
								<tr>
									<td>${boughtDataDetail.formatDate}</td>
									<td>${boughtDataDetail.deliveryMethodName}</td>
									<td>${boughtDataDetail.totalPrice + boughtDataDetail.deliveryMethodPrice} 円</td>
                                   <td >あああ
                                    </td>
                                      <td >あああ
                                    </td>
                                    <td >
                                    <a href="" class="btn btn--yellow btn--cubic">詳細</a>                           </td>
								</tr>
                            </tbody>
						</table>
        <br>

			<h3 class="offset-sm-4">チーム向け情報ランダム表示</h3>

						<table class="table">
							<thead>
								<tr>
                                    <th scope="col">募集内容</th>
									<th scope="col">利用日時</th>
									<th scope="col"> チーム</th>
                                    <th scope="col">場所</th>
									<th scope="col">募集人数</th>
                                    <th scope="col">詳細</th>

								</tr>

							</thead>
							<tbody>
								<tr>
									<td>${boughtDataDetail.formatDate}</td>
									<td>${boughtDataDetail.deliveryMethodName}</td>
									<td>${boughtDataDetail.totalPrice + boughtDataDetail.deliveryMethodPrice} 円</td>
                                    <td>あああ</td>
                                      <td>あああ
                                    </td>
                                    <td>
                                    <a href="" class="btn btn--yellow btn--cubic">詳細</a>
                                    </td>
								</tr>
                            </tbody>
						</table>
    </body>

    <br>
       <footer>
           <div class="offset-sm-1">
        <a href="4">ログアウト</a>
      </div>
       </footer>

    </html>