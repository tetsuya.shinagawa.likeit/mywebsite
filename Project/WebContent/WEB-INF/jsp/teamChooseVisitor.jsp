<%@	page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<meta charset="UTF-8">
    <head>
<title>申し込みユーザ情報
    </title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      </head>

<body>

	<div class="container row offset-md-4">
	<header>
        <h3>申し込みプレイヤー一覧</h3>
    </header>
        </div>

						<table class="table">
							<thead>
								<tr>
                                    <th scope="col">#</th>
									<th scope="col">名前</th>
									<th scope="col">ポジション</th>
                                    <th scope="col">所属チーム</th>
                                    <th scope="col">年齢</th>
                                    <th>プレイヤー情報詳細</th>
                                </tr>
                        </thead>
                            <tbody>
								<tr>
                                    <th scope="row">1</th>
									<td >${boughtDataDetail.formatDate}</td>
									<td>${boughtDataDetail.deliveryMethodName}</td>
                                    <td>${boughtDataDetail.totalPrice}</td>
                                    <td>備考</td>
                                    <td>
                                        <button value="編集" class="btn btn-primary form-submit">プレイヤー情報詳細</button>
                                    <td><input type="checkbox" name="tekito"></td>
								</tr>
                           </tbody>
						</table>


                 <div class="container-sm">
                     <div class="row">
                     <div class="offset-2">
                        <button class="btn btn-warning form-submit">
                        選んだプレイヤーを確定する</button>
                        </div>
                     </div>
    </div>

    	<div class="container row offset-md-4">
	<header>
        <br>


        <h3>確定したプレイヤー</h3>
    </header>
        </div>

						<table class="table">
							<thead>
								<tr>
                                    <th scope="col">#</th>
									<th scope="col">名前</th>
									<th scope="col">ポジション</th>
                                    <th scope="col">所属チーム</th>
                                    <th scope="col">年齢</th>
                                    <th>プレイヤー情報詳細</th>
                                    <th>出欠確認</th>
                                </tr>
                        </thead>
                            <tbody>
								<tr>
                                    <th scope="row">1</th>
									<td >${boughtDataDetail.formatDate}</td>
									<td>${boughtDataDetail.deliveryMethodName}</td>
                                    <td>${boughtDataDetail.totalPrice </td>
                                    <td>備考</td>
                                    <td>
                                        <button value="編集" class="btn btn-primary form-submit">プレイヤー情報詳細</button>
                                    </td>

                                       <td>
                                    <form>
                                        <div class="form-row align-items-center">
                                            <div class="col-auto my-1">
                                                <label class="mr-sm-2 sr-only" for="inlineFormCustomSelect">Preference</label>
                                                <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                                    <option value="出席">出席</option>
                                                    <option value="欠席">欠席</option>
                                                    <option value="その他">その他</option>
                                                </select>
                                            </div>
                                            </div>
                                            </form>
                                           </td>
                                            </tr>
                           </tbody>
                             </table>

                                <div class="container-sm">
                     <div class="row">
                     <div class="offset-2">
                        <button class="btn btn-info form-submit">
                        出席情報を更新する</button>
                        </div>
                     </div>
    </div>
    </body>
    <br>
			<footer>
                <div class="container-sm">
                <div class="offset-2">
                <a href="">戻る</a>
                </div>
                </div>
    </footer>

</html>