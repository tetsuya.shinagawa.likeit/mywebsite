<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報詳細参照</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

	<body class=container>
            <div class="row">
                <div class="offset-4">
                    <h1>登録完了</h1>
                </div>
        </div>

         <div class="row">
             <div class="col-6">
             <button type="button" class="btn btn-primary">そのままログインする</button>
                 </div>

    <div class="col-6">
        <button type="button" class="btn btn-outline-secondary">ログイン画面へ戻る</button>
        </div>

        </div>

    </body>

</html>

