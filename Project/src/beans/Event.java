package beans;

import java.io.Serializable;

public class Event implements Serializable{
	private int eventId;
	private String title;
	private String eventTime;
	private String  requestedNumbers;
	private String detail;
	private String location;
	private int userId;
	private int attendance;


	public Event (int eventId, String title, String eventTime,String requestedNumbers, String detail,String location) {
	this.eventId = eventId;
	this.title= title;
	this.eventTime= eventTime;
	this.requestedNumbers= requestedNumbers;
	this.location=location;
	}

	public Event(int eventId, int userId, String title, String eventTime, String location, int attendance,
			String remarks) {
		this.eventId=eventId;
		this.userId=userId;
		this.title = title;
		this.eventTime = eventTime;
		this.location = location;
		this.attendance = attendance;
	}



	public Event(String listLocation, String listName, String listEventTime, int listRequestedNumbers) {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getAttendance() {
		return attendance;
	}

	public void setAttendance(int attendance) {
		this.attendance = attendance;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public String getRequestedNumbers() {
		return requestedNumbers;
	}

	public void setRequestedNumbers(String requestedNumbers) {
		this.requestedNumbers = requestedNumbers;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}


	}