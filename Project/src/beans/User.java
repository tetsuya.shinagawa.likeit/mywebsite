package beans;

import java.io.Serializable;



public class User implements Serializable {
private int id;
private String loginId;
private String name;
private String teamName;
private String password;
private String gender;
private String email;
private int userId;



// ログインセッションを保存するためのコンストラクタ
public User(String loginId, String name) {
	this.loginId = loginId;
	this.name = name;
}


public User(int userId, String loginId, String name,String teamName,String password,String gender,String email) {
	this.userId = userId;
	this.loginId = loginId;
	this.name = name;
	this.teamName=teamName;
	this.password = password;
	this.gender= gender;
	this.email= email;
}



public int getsUerId() {
	return userId;
}

public void setUserId(int userId) {
	this.userId = userId;
}

public String getLoginId() {
	return loginId;
}

public void setLoginId(String loginId) {
	this.loginId = loginId;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getRelatedTeam() {
	return teamName;
}

public void setRelatedTeam(String relatedTeam) {
	this.teamName = relatedTeam;
}

public String getPassword() {
	return password;
}

public void setPassword(String password) {
	this.password = password;
}

public String getGender() {
	return gender;
}

public void setGender(String gender) {
	this.gender = gender;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}
}

