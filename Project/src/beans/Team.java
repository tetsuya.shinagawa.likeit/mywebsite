package beans;

import java.io.Serializable;

public class Team implements Serializable{
	private String loginId;
	private String password;
	private int teamId;
	private String teamName;
	private String email;
	private String mainMember;
	private String mainMember2;
	private String mainMember3;

	public Team(int teamId,String password, String teamName, String email, String mainMember, String mainMember2,
			String mainMember3, String loginId) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.teamId = teamId;
		this.password = password;
		this.teamName= teamName;
		this.email = email;
		this.mainMember = mainMember;
		this.mainMember2 = mainMember2;
		this.mainMember3 = mainMember3;
		this.loginId = loginId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMainMember() {
		return mainMember;
	}

	public void setMainMember(String mainMember) {
		this.mainMember = mainMember;
	}

	public String getMainMember2() {
		return mainMember2;
	}

	public void setMainMember2(String mainMember2) {
		this.mainMember2 = mainMember2;
	}

	public String getMainMember3() {
		return mainMember3;
	}

	public void setMainMember3(String mainMember3) {
		this.mainMember3 = mainMember3;
	}

}
