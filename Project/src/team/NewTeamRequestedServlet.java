package team;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Event;
import dao.EventDao;

public class NewTeamRequestedServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewTeamRequestedServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");


		String title = request.getParameter("title");
		String teamName = request.getParameter("teamName");
		String location = request.getParameter("location");
		String dateTime = request.getParameter("dateTime");
		String email = request.getParameter("email");
		String detail = request.getParameter("detail");

		EventDao eventDao = new EventDao();


		if(title.equals("")|teamName.equals("")||location.equals("")||dateTime.equals("")||email.equals("")||detail.equals("")){

			request.setAttribute("errMsg", "未記入の項目があります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDetailEdit.jsp");
			dispatcher.forward(request, response);
			return;
			}

		else {
			Event event = eventDao.insertNewEvent(title,teamName,location,dateTime,email,detail);
			request.setAttribute("event", event);
			response.sendRedirect("TeamMainServlet");
		}

	}

}
