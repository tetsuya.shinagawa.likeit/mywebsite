package team;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

public class TeamInfoEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */

        // TODO Auto-generated constructor stub

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User log = (User)session.getAttribute("log");

		if(log!=null);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String teamName = request.getParameter("teamName");
		String mainMember = request.getParameter("mainMember");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String email = request.getParameter("email");
		String profile = request.getParameter("profile");

		UserDao userDao = new UserDao();

		request.setAttribute(password,"password");
		String code = userDao.generate(password);

		request.setAttribute(password2,"password2");
		String codeConf = userDao.generate(password);

		if(!password.equals(password2)) {
			request.setAttribute("errMsg", "パスワードが一致しません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
		else if(teamName.equals("")|loginId.equals("")||email.equals("")) {
			request.setAttribute("errMsg", "未記入の項目があります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUpdate.jsp");
			dispatcher.forward(request, response);
			return;
			}

		else if(password.equals("")) {
			User user = userDao.updateUserInfo(teamName,loginId,email);
			request.setAttribute("user", user);
			response.sendRedirect("TeamDetailConfServlet");

		}
		else {

		User user = userDao.updateUserInfoWithPassword(code,teamName,loginId,email);

		request.setAttribute("user", user);
		response.sendRedirect("TeamDetailConfServlet");

		}

		}

}
