package team;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Event;
import dao.EventDao;

public class TeamRequestedListServlet  extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TeamRequestedListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

			String id= request.getParameter("teamId");
			int teamId = Integer.parseInt(id);

		// ユーザ一覧情報を取得
		EventDao eventDao = new EventDao();
		List<Event> eventList = eventDao.findEventByTeamId(teamId);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("eventList", eventList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/teamRequestedList.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		}

}
