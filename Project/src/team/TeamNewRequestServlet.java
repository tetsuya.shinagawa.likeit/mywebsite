package team;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EventDao;
import dao.UserDao;

public class TeamNewRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TeamNewRequestServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 *
	 */

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewUser.jsp");
		dispatcher.forward(request, response);


	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");


		String title = request.getParameter("title");
		String teamName = request.getParameter("teamName");
		String dateTime = request.getParameter("dateTime");
		String location = request.getParameter("location");
		String email = request.getParameter("email");
		String remarks = request.getParameter("remarks");
		String detail = request.getParameter("detail");

		EventDao eventDao = new EventDao();

		if(dateTime !=null) {
			request.setAttribute("errMsg", "同時刻にイベントが登録されています。");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/teamRequestVisitor.jsp");
			dispatcher.forward(request, response);
			return;
		}
		else if(title.equals("")||teamName.equals("")||dateTime.equals("")||location.equals("")||remarks.equals("")||detail.equals("")) {
			request.setAttribute("errMsg", "入力されていない情報があります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/teamRequestVisitor.jsp");
			dispatcher.forward(request, response);
			return;
		}

		else {
		String event = eventDao.makeNewEventById(title,teamName,dateTime,location,email,remarks,detail);
		request.setAttribute("event",event);
		response.sendRedirect("teamRequestVisitorConfServlet");

	}
	}
}
