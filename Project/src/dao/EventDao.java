package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.Event;

/**
 *
 * @author d-yamaguchi
 *
 */
public class EventDao{

	public String makeNewEventId(int randomNum) {
		Connection con= null;
		try { con = DBManager.getConnection();

	    // SELECT文を準備
	    String sql = "SELECT * FROM roster_event WHERE event_id = ?";

	     // SELECTを実行し、結果表を取得
	    PreparedStatement ps = con.prepareStatement(sql);
		   ps.setInt(1, randomNum);
		   ResultSet rs = ps.executeQuery();

	    // randomNumに値が入っていた場合、ID被りのためTeamRequestVisitorServletへ作り直し要求
		   if(!rs.equals(null)) {

		   }
		   else if(!rs.next()) {

	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    return null;
	} finally {
	    // データベース切断
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }
	    }
	}
		return null;
		}

}

	 public List<Event> findAll() {
	        Connection con = null;

	        List<Event>eventList= new ArrayList<Event>();
	        try {
	            // データベースへ接続
	            con = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM roster_event WHERE event_id = ?";



	             // SELECTを実行し、結果表を取得
	           Statement stmt = con.createStatement();
	           ResultSet rs= stmt.executeQuery(sql);
	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	            while (!rs.next()) {
	            	int eventId= rs.getInt("event_id");
	            	String title= rs.getString("title");
	            	String eventTime= rs.getString("event_title");
	            	String requestedNumbers = rs.getString("requested_numbers");
	            	String location = rs.getString("location");

	            }

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (con != null) {
	                try {
	                    con.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
			return null;

	}

	public Event findUserHistoryById(int userId, int eventId) {
		Connection con= null;
	try { con = DBManager.getConnection();

    // SELECT文を準備
    String sql = "SELECT * FROM roster_event WHERE event_id = ?";

     // SELECTを実行し、結果表を取得
   Statement stmt = con.createStatement();
   ResultSet rs= stmt.executeQuery(sql);
    // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う

    if(!rs.next()) {
    }

} catch (SQLException e) {
    e.printStackTrace();
    return null;
} finally {
    // データベース切断
    if (con != null) {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
	return null;
	}


	public String findTeamRequestById() {
		Connection con= null;

		List<Event>eventList = new ArrayList<Event>();

		try {
			con = DBManager.getConnection();

	    // SELECT文を準備
	    String sql = "SELECT * FROM roster_event WHERE event_id = ?";

	     // SELECTを実行し、結果表を取得
	   Statement stmt = con.createStatement();
	   ResultSet rs= stmt.executeQuery(sql);
	    // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う

	    if(!rs.next()) {
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    return null;
	} finally {
	    // データベース切断
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }
}
	}
		return null;
	}

	public List<Event> searchEvent(String location,String teamName,String eventTime,int requestedNumbers) {
		Connection con= null;

		List<Event> eventList = new ArrayList<Event>();

        try {
        	String sql ="SELECT * FROM user WHERE login_id not in('admin')";

        	if(!location.equals("")) {
        		sql+=" AND login_id= "+"'"+location+"'";
        	}
        	if(!teamName.equals("")) {
        		sql+=" AND name LIKE "+"'"+ "%"+teamName+"%" +"'";

        	}
        	if(!eventTime.equals("")) {
        		sql+=" AND birth_date >= "+"'"+eventTime+"'";
        	}
        	if(!requestedNumbers=="") {
        		sql+=" AND birth_date < "+"'"+requestedNumbers+"'";
        	}


        	System.out.println(sql);

        	 con = DBManager.getConnection();

	     // SELECTを実行し、結果表を取得
	   Statement st = con.createStatement();
	   ResultSet rs = st.executeQuery(sql);


	    while(!rs.next()) {

	    	String listLocation = rs.getString("location");
	    	String listName = rs.getString("teamName");
	    	String listEventTime = rs.getString("event_time");
	    	int listRequestedNumbers= rs.getInt("requested_numbers");

	    	Event event = new Event(listLocation,listName,listEventTime,listRequestedNumbers);
	    	eventList.add(event);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    return null;
	} finally {
	    // データベース切断
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }
	}
	}
		return null;
		}

	public Event findEventById() {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	public Event insertNewEvent(String title, String teamName, String location, String dateTime, String email,
			String detail) {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	public List<Event> findEventByTeamId(int teamId) {
		Connection con= null;

		List<Event> eventList = new ArrayList<Event>();

        try {
        	String sql ="SELECT * FROM roster_event WHERE team_id=?";

        	System.out.println(sql);

        	 con = DBManager.getConnection();

	     // SELECTを実行し、結果表を取得
	   PreparedStatement ps = con.prepareStatement(sql);
	   ps.setInt(1, teamId);
	   ResultSet rs = ps.executeQuery();


	    while(!rs.next()) {

	    	String location = rs.getString("location");
	    	String teamName = rs.getString("teamName");
	    	String eventTime = rs.getString("event_time");
	    	int requestedNumbers= rs.getInt("requested_numbers");


	    	Event event = new Event(location,teamName,eventTime,requestedNumbers);
	    	eventList.add(event);
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    return null;
	} finally {
	    // データベース切断
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }
	}
	}
		return null;
		}

	public String makeNewEventById(String title, String teamName, String dateTime, String location, String email,
			String remarks, String detail) {
		Connection con= null;
		try { con = DBManager.getConnection();

	    // SELECT文を準備
	    String sql = "SELECT * FROM roster_event WHERE event_id = ?";

	     // SELECTを実行し、結果表を取得
	    PreparedStatement ps = con.prepareStatement(sql);
		   ps.setInt(1, eventId);
		   ResultSet rs = ps.executeQuery();

	    // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う

	    if(!rs.next()) {
	    }

	} catch (SQLException e) {
	    e.printStackTrace();
	    return null;
	} finally {
	    // データベース切断
	    if (con != null) {
	        try {
	            con.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }
	    }
	}
		return null;
		}

}
