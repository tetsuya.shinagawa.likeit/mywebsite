package dao;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CodeDao {
	 public String generate(String password) {
         MessageDigest md;
         String result ="" ;
			try {
				md = MessageDigest.getInstance("MD5");

         md.update(password.getBytes());
         byte[] digest = md.digest();
          result = new BigInteger(1, digest).toString(16).toUpperCase();

			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			return result;
     }

}
