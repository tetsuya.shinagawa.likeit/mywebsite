package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.Team;

public class TeamDao{

	public String makeNewTeamInfo(String teamId, String teamName, String email, String code, String mainMember, String mainMember2,String mainMember3,String loginId) {
		Connection con = null;
		try {
			String sql ="insert into roster_team \r\n" +
					"(team_id,team_name,password,email,main_member,main_member2,main_member3,login_id)\r\n" +
					"Values('?','?','?','?','?','?','?','?');";

            con = DBManager.getConnection();   //Connectionクラス生成

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, teamId);
            ps.setString(2, teamName);
            ps.setString(3, code);
            ps.setString(4, email);
            ps.setString(5, mainMember);
            ps.setString(6, mainMember2);
            ps.setString(7, mainMember3);
            ps.setString(8, loginId);
            ResultSet rs = ps.executeQuery();

                // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
                if (!rs.next()) {

                    return null;
                }

                // 必要なデータのみインスタンスのフィールドに追加
                String teamLoginId = rs.getString("login_id");

                return teamLoginId;


            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            } finally {
                // データベース切断
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        return null;
                    }
                }
            }
	}

	public Team findTeamByLoginInfo(String loginId) {
		Connection con = null;
		try {
			String sql ="Select * from roster_team where login_id='?'";

            con = DBManager.getConnection();   //Connectionクラス生成

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, loginId);
            ResultSet rs = ps.executeQuery();

                // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
                if (!rs.next()) {
                    return null;
                }

                // 必要なデータのみインスタンスのフィールドに追加
                int teamId = rs.getInt("team_id");
                String password = rs.getString("password");
                String teamName = rs.getString("name");
                String email= rs.getString("email");
                String mainMember = rs.getString("mainMember1");
                String mainMember2= rs.getString("mainMember2");
                String mainMember3= rs.getString("mainMember3");
                String teamLoginId = rs.getString("loginId");

                return new Team( teamId,password,teamName,email,mainMember,mainMember2,mainMember3,teamLoginId);


            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            } finally {
                // データベース切断
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        return null;
                    }

		}
		// TODO 自動生成されたメソッド・スタブ
		return null;
            }
            }

}
