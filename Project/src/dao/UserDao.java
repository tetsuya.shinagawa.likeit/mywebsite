package dao;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.Team;
import beans.User;

/**
 *
 * @author d-yamaguchi
 *
 */
public class UserDao{


	 public User findByLoginInfo(String loginId, String code) {
	        Connection con = null;
	        try {
	            // データベースへ接続
	            con = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement ps = con.prepareStatement(sql);
	            ps.setString(1, loginId);
	            ps.setString(2, code);
	            ResultSet rs = ps.executeQuery();
	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	            if (!rs.next()) {

	            // 必要なデータのみインスタンスのフィールドに追加
	            	int id = rs.getInt("id");
	            String name = rs.getString("name");
	            String relatedTeam = rs.getString("related_team");
	            String gender = rs.getString("gender");
	            String email = rs.getString("email");
	            return new User(id,loginId,code,name,relatedTeam,gender,email);
	            }

	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (con != null) {
	                try {
	                    con.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
			return null;

	}

	 public String generate(String password) {
         MessageDigest md;
         String result ="" ;
			try {
				md = MessageDigest.getInstance("MD5");

         md.update(password.getBytes());
         byte[] digest = md.digest();
          result = new BigInteger(1, digest).toString(16).toUpperCase();

			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			return result;
     }

	public String findTeamDataByLoginId(String loginId) {
		Connection con= null;
		try {
			String sql ="SELECT login_id FROM user WHERE login_id = ?";

            con = DBManager.getConnection();   //Connectionクラス生成

                 // SELECTを実行し、結果表を取得
                PreparedStatement pStmt = con.prepareStatement(sql);
                pStmt.setString(1, loginId);
                ResultSet rs = pStmt.executeQuery();

                // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
                if (!rs.next()) {
                    return null;
                }

                // 必要なデータのみインスタンスのフィールドに追加
                String loginIdList = rs.getString("login_id");


                return loginIdList;


            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            } finally {
                // データベース切断
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        return null;
                    }

		}
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

	}

		public Team findTeamByLoginInfo(String loginId) {
			Connection con = null;
			try {
				String sql ="Select * from roster_team where login_id='?'";

	            con = DBManager.getConnection();   //Connectionクラス生成

	            PreparedStatement ps = con.prepareStatement(sql);
	            ps.setString(1, loginId);
	            ResultSet rs = ps.executeQuery();

	                // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
	                if (!rs.next()) {
	                    return null;
	                }

	                // 必要なデータのみインスタンスのフィールドに追加
	                int teamId = rs.getInt("team_id");
	                String password = rs.getString("password");
	                String teamName = rs.getString("name");
	                String email= rs.getString("email");
	                String mainMember = rs.getString("mainMember1");
	                String mainMember2= rs.getString("mainMember2");
	                String mainMember3= rs.getString("mainMember3");
	                String teamLoginId = rs.getString("loginId");

	                return new Team( teamId,password,teamName,email,mainMember,mainMember2,mainMember3,teamLoginId);


	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            } finally {
	                // データベース切断
	                if (con != null) {
	                    try {
	                        con.close();
	                    } catch (SQLException e) {
	                        e.printStackTrace();
	                        return null;
	                    }

			}
			// TODO 自動生成されたメソッド・スタブ
			return null;
		}
	}

		public User findUserByLoginId(String loginId) {

			Connection con = null;
			try {
				String sql ="Select * from roster_user where login_id='?'";

	            con = DBManager.getConnection();   //Connectionクラス生成

	            PreparedStatement ps = con.prepareStatement(sql);
	            ps.setString(1, loginId);
	            ResultSet rs = ps.executeQuery();

	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
                if (!rs.next()) {
                    return null;
                }

	            int userId = rs.getInt("user_id");
	            String loginId2 = rs.getString("login_id");
	            String name = rs.getString(("name"));
                String password = rs.getString("password");
                String teamName = rs.getString("teamName");
                String gender = rs.getString("gender");
                String email= rs.getString("email");

                return new User(userId,loginId2,name,password,teamName,gender,email) ;


			 } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            } finally {
	                // データベース切断
	                if (con != null) {
	                    try {
	                        con.close();
	                    } catch (SQLException e) {
	                        e.printStackTrace();
	                        return null;
	                    }

	                }
		}

		}

		public User updateUserInfo(String name, String loginId, String email) {
			// TODO 自動生成されたメソッド・スタブ
			Connection con = null;
			try {
				String sql ="Select * from roster_user where login_id='?'";

	            con = DBManager.getConnection();   //Connectionクラス生成

	            PreparedStatement ps = con.prepareStatement(sql);
	            ps.setString(1, name);
	            ps.setString(2, loginId);
	            ps.setString(3, email);
	            ResultSet rs = ps.executeQuery();

	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
                if (!rs.next()) {
                    return null;
                }

	            int userId = rs.getInt("user_id");
	            String upLoginId = rs.getString("login_id");
	            String upName = rs.getString(("name"));
                String password = rs.getString("password");
                String teamName = rs.getString("teamName");
                String gender = rs.getString("gender");
                String upEmail= rs.getString("email");

                return new User(userId,upLoginId,upName,password,teamName,gender,upEmail) ;


			 } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            } finally {
	                // データベース切断
	                if (con != null) {
	                    try {
	                        con.close();
	                    } catch (SQLException e) {
	                        e.printStackTrace();
	                        return null;
	                    }


}
	            }
		}

		public User updateUserInfoWithPassword(String code, String name, String loginId, String email) {
			// TODO 自動生成されたメソッド・スタブ
			Connection con = null;
			try {
				String sql ="Select * from roster_user where login_id='?'";

	            con = DBManager.getConnection();   //Connectionクラス生成

	            PreparedStatement ps = con.prepareStatement(sql);
	            ps.setString(1, code);
	            ps.setString(2, name);
	            ps.setString(3, loginId);
	            ps.setString(4, email);
	            ResultSet rs = ps.executeQuery();

	            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
                if (!rs.next()) {
                    return null;
                }

	            int userId = rs.getInt("user_id");
	            String upLoginId = rs.getString("login_id");
	            String upName = rs.getString(("name"));
                String upCode = rs.getString("upCode");
                String teamName = rs.getString("teamName");
                String gender = rs.getString("gender");
                String upEmail= rs.getString("email");

                return new User(userId,upLoginId,upName,upCode,teamName,gender,upEmail) ;


			 } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            } finally {
	                // データベース切断
	                if (con != null) {
	                    try {
	                        con.close();
	                    } catch (SQLException e) {
	                        e.printStackTrace();
	                        return null;
	                    }

		}
	    }
		}

		public String makeNewUserInfo(String userId, String teamName, String email, String code, String mainMember,
				String mainMember2, String mainMember3, String loginId) {
			// TODO 自動生成されたメソッド・スタブ
			return null;
		}
}
