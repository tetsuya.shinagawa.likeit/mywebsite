package user;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.CodeDao;
import dao.UserDao;

public class UserDetailEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */

        // TODO Auto-generated constructor stub

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User log = (User)session.getAttribute("log");

		if(log!=null);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetailEdit.jsp");
		dispatcher.forward(request, response);

		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String teamName = request.getParameter("teamName");
		String email = request.getParameter("email");
		String profile = request.getParameter("profile");

		CodeDao codeDao= new CodeDao();

		request.setAttribute(password,"password");
		String code = codeDao.generate(password);

		request.setAttribute(password2,"password2");
		String codeConf = codeDao.generate(password2);

		UserDao userDao= new UserDao();


		if(!password.equals(password2)) {
			request.setAttribute("errMsg", "パスワードが一致しません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDetailEdit.jsp");
			dispatcher.forward(request, response);
			return;
		}
		else if(name.equals("")|loginId.equals("")||email.equals("")) {
			request.setAttribute("errMsg", "未記入の項目があります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserDetailEdit.jsp");
			dispatcher.forward(request, response);
			return;
			}

		else if(password.equals("")) {
			User user = userDao.updateUserInfo(name,loginId,email);
			request.setAttribute("user", user);
			response.sendRedirect("UserMainServlet");

		}
		else {

		User user = userDao.updateUserInfoWithPassword(code,name,loginId,email);

		request.setAttribute("user", user);
		response.sendRedirect("UserDetailServlet");

		}

		}


}
