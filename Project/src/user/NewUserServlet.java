package user;


	import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;


	/**
	 * Servlet implementation class NewUser
	 */
	@WebServlet("/NewUserServlet")
	public class NewUserServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

	    /**
	     * @see HttpServlet#HttpServlet()
	     */
	    public NewUserServlet() {
	        super();
	        // TODO Auto-generated constructor stub
	    }

		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 *
		 */

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			request.setCharacterEncoding("UTF-8");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewUser.jsp");
			dispatcher.forward(request, response);


		}


		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response)
				throws ServletException, IOException {
			request.setCharacterEncoding("UTF-8");

			String userId = request.getParameter("userId");
			String password = request.getParameter("password");
			String password2 = request.getParameter("password2");
			String mainMember = request.getParameter("mainMember");
			String mainMember2 = request.getParameter("mainMember2");
			String mainMember3 = request.getParameter("mainMember3");
			String teamName = request.getParameter("teamName");
			String email= request.getParameter("email");
			String loginId = request.getParameter("loginId");

			//パスワードをmd５で暗号化
			request.setAttribute(password,"password");
			UserDao userDao = new UserDao();
			String code = userDao.generate(password);

			String userLoginId = userDao.findTeamDataByLoginId(loginId);

			if (!password.equals(password2)) {
				request.setAttribute("errMsg", "パスワードが一致しません");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewUser.jsp");
				dispatcher.forward(request, response);
				return;
			}

			else if(userLoginId !=null) {
				request.setAttribute("errMsg", "すでに存在するログインID");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewUser.jsp");
				dispatcher.forward(request, response);
				return;
			}
			else if(loginId.equals("")||password.equals("")||teamName.equals("")||email.equals("")||mainMember.equals("")) {
				request.setAttribute("errMsg", "入力されていない情報があります");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewUser.jsp");
				dispatcher.forward(request, response);
				return;
			}

			else {
			String user = userDao.makeNewUserInfo(userId,teamName,email,code,mainMember, mainMember2, mainMember3,loginId);

			request.setAttribute("user",user);
			response.sendRedirect("newUserConf.jsp");
		}
		}
	}
