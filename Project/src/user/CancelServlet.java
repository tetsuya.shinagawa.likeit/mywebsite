package user;



import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Event;
import beans.User;
import dao.EventDao;

public class CancelServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;


    /**
     * @see HttpServlet#HttpServlet()
     */

        // TODO Auto-generated constructor stub

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		User log = (User)session.getAttribute("log");

		if(log!=null);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("userId");
		int userId = Integer.parseInt(id);
		String id2 = request.getParameter("eventId");
		int eventId = Integer.parseInt(id2);

		EventDao eventDao = new EventDao();
		Event event = eventDao.findUserHistoryById(userId,eventId);

		request.setAttribute("event", event);
		response.sendRedirect("cancelServletConfServlet");
		}

		}