package user;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EventDao;

public class VisitorInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public VisitorInfoServlet() {
		super();
	}



	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる

		EventDao eventDao = new EventDao();
		String eventList= eventDao.findTeamRequestById();

		request.setAttribute("eventList", eventList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/visitorInfoSearch.jsp");
		dispatcher.forward(request, response);


        //申し込みボタンを押したらvistorApplicationComplete.jspへ遷移するようにする。

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

	}
}
